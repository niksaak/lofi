use std::sync::atomic::{Ordering, AtomicUsize};
use lofi::{self, state, Executor};

#[test]
fn executor_empty_tick() {
    let _executor = Executor::new();
}

#[test]
fn spawn_one_noop() {
    let mut executor = Executor::new();
    executor.insert(async {});
    executor.tick(&mut ());
}

#[test]
fn spawn_one_yield_once() {
    let mut executor = Executor::new();
    executor.insert(async { lofi::yield_now().await; });
    for _ in 0..2 {
        executor.tick(&mut ());
    }
}

#[test]
fn spawn_one_spawner() {
    let mut executor = Executor::new();
    executor.insert(async move {
        state::spawn(async {});
        lofi::yield_now().await;
    });
    for _ in 0..2 {
        executor.tick(&mut ());
    }
}

#[test]
fn spawn_with_state() {
    let mut executor = Executor::new();
    let counter = AtomicUsize::new(0);
    executor.insert(async move {
        state::with(|state: &aux::State| {
            state.0.fetch_add(1, Ordering::Relaxed);
        });
        lofi::yield_now().await;
        state::with(|state: &aux::State| {
            state.0.fetch_add(1, Ordering::Relaxed);
        });
        lofi::yield_now().await;
    });

    executor.tick(&mut aux::State(&counter));
    assert_eq!(counter.load(Ordering::Relaxed), 1);
    executor.tick(&mut aux::State(&counter));
    assert_eq!(counter.load(Ordering::Relaxed), 2);
}

#[test]
fn spawn_with_state_exclusive() {
    let mut executor = Executor::new();
    let mut counter = 0_usize;
    executor.insert(async move {
        state::with_mut(|state: &mut aux::StateExclusive| {
            *state.0 += 1;
        }).await;
    });
    executor.tick(&mut aux::StateExclusive(&mut counter));
    assert_eq!(counter, 1);
}

mod aux {
    use std::{
        sync::atomic::AtomicUsize,
    };
    use lofi::impl_universal_type_id;

    pub struct State<'a>(pub &'a AtomicUsize);
    pub struct StateExclusive<'a>(pub &'a mut usize);

    impl_universal_type_id!(State<'a>);
    impl_universal_type_id!(StateExclusive<'a>);
}
