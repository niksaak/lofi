use std::{
    lazy::SyncLazy,
    ptr::null,
    task::{Context, RawWaker, RawWakerVTable, Waker},
};

pub fn noop_context() -> Context<'static> {
    Context::from_waker(&NOOP_WAKER)
}

pub fn noop_waker() -> Waker {
    unsafe {
        Waker::from_raw(noop_raw_waker())
    }
}

unsafe fn noop_clone(_: *const ()) -> RawWaker {
    noop_raw_waker()
}

unsafe fn noop(_: *const ()) {}

const NOOP_WAKER_VTABLE: RawWakerVTable = RawWakerVTable::new(noop_clone, noop, noop, noop);
static NOOP_WAKER: SyncLazy<Waker> = SyncLazy::new(noop_waker);

const fn noop_raw_waker() -> RawWaker {
    RawWaker::new(null(), &NOOP_WAKER_VTABLE)
}

