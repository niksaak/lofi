use std::any::TypeId;
use crate::universal_type_id::UniversalTypeId;

#[derive(Clone, Copy)]
pub(crate) struct UncheckedAny {
    datum: *mut (),
    type_id: TypeId,
}

impl UncheckedAny {
    pub unsafe fn new<T: UniversalTypeId>(thing: &mut T) -> Self {
        Self {
            datum: thing as *mut T as *mut (),
            type_id: thing.universal_type_id_of(),
        }
    }

    pub unsafe fn downcast_ref_unchecked<T: UniversalTypeId>(self) -> *mut T {
        if self.type_id == T::universal_type_id() {
            self.datum as *mut () as *mut T
        } else {
            panic!("Attempt to downcast to incompatible type")
        }
    }
}

