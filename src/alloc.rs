use std::{
    alloc::{self, Layout},
    marker::Unsize,
    mem,
    ops::{Deref, DerefMut, CoerceUnsized},
    pin::Pin,
    ptr,
    sync::atomic::{AtomicU64, AtomicU32, Ordering},
};
use bumpalo::{self, Bump, boxed::Box as BBox};

// Chunk is a 512 byte array.
// Map is a 64-bit word:
//
// ffff ffff ffff ffff ffff ffff ffff ffff //  16 // 2^4 - 5th order
// e e  e e  e e  e e  e e  e e  e e  e e  //  32 //   5 - 4th order
// d    d    d    d    d    d    d    d    //  64 //   6 - 3th order
// c         c         c         c         // 128 //   7 - 2th order
// b                   b                   // 256 //   8 - 1th order
// a                                       // 512 //   9 - 0th order
//
// 00
// 01                                              02
// 03                      04                      05                      06...
// 07          08          09          10          11          12          13...
// 15    16    17    18    19    20    21    22    23    24    25    26    27...
// 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55...
// -----------------------------------------------------------------------------
// 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24...

const CHUNK_SIZE: usize = 1024;
const BLOCK_MIN_POT: usize = 5;
const LAST_ORDER: usize = 5;

pub struct Chunker {
    alloc: &'static mut Bump,
    maps: Vec<&'static AtomicU64>,
    chunks: Vec<ptr::NonNull<u8>>,
}

pub struct CBox<T: ?Sized> {
    map: &'static AtomicU64,
    dealloc_bits: u64,
    datum: ptr::NonNull<T>,
}

pub struct CArc<T: Send + Sync>(ptr::NonNull<CArcInner<T>>);

pub struct CWeak<T: Send + Sync>(ptr::NonNull<CArcInner<T>>);

struct CArcInner<T: Send + Sync> {
    map: &'static AtomicU64,
    dealloc_bits: u64,
    strong: AtomicU32,
    weak: AtomicU32,
    datum: T,
}

impl Chunker {
    pub fn new() -> Self {
        Self {
            alloc: Box::leak(Box::new(Bump::new())),
            maps: Vec::with_capacity(16),
            chunks: Vec::with_capacity(16),
        }
    }

    pub fn alloc_raw(&mut self, layout: Layout) -> (ptr::NonNull<u8>, &'static AtomicU64, u64) {
        let size_pot = (1_usize.leading_zeros() - layout.size().next_power_of_two().leading_zeros()) as usize;
        let align_pot = (1_usize.leading_zeros() - layout.align().next_power_of_two().leading_zeros()) as usize;
        let pot = usize::max(size_pot, align_pot);

        let reverse_order = pot.saturating_sub(BLOCK_MIN_POT);
        let (order, underflow) = LAST_ORDER.overflowing_sub(reverse_order);
        if underflow {
            panic!("Allocation is too big: {}", reverse_order);
        }

        loop {
            for (map, chunk) in self.maps.iter().zip(self.chunks.iter()) {
                if layout.size() == 0 {
                    return (ptr::NonNull::<u8>::dangling(), map, 0);
                }

                // Allocate in bitmap.
                let map_value = map.load(Ordering::SeqCst);
                let level = map_value | ORDER_NOT_BITS[order];
                if level == u64::MAX &! (1<<63) {
                    continue;
                }
                let index = (level.trailing_ones() % 63) as usize;
                map.fetch_or(ALLOCATION_BITS[index], Ordering::SeqCst);

                // The node bit and it's children to be marked free on dealloc.
                let dealloc_bits = ALLOCATION_BITS[index] & ORDER_CHILDREN_BITS[order];

                // Offset in chunk.
                let mut offset = index;
                offset -= (ORDER_NOT_BITS[order].trailing_ones() % 63) as usize;
                offset <<= LAST_ORDER - order;
                offset <<= BLOCK_MIN_POT;

                // Reference to where the allocated thing is stored.
                // SAFETY: the code makes following assumptions:
                // + chunk points to an uninitialized array 512B in size,
                // + offset is a 2^BLOCK_MIN_POT-aligned index inside that array.
                let ptr = unsafe {
                    let ptr = chunk.as_ptr().offset(offset as isize);
                    let ptr = ptr::NonNull::new(ptr).unwrap();
                    ptr
                };

                return (ptr, map, dealloc_bits);
            }

            // No available chunks to allocate in, make more.
            // SAFETY:
            // + Leaking `bumpalo::boxed::Box` is safe because `self.alloc` is leaked.
            // + Unchecked layout creation is safe as we're using a non-zero constant.
            // + Call to `alloc` is safe because layout is non-zero size due to above.
            unsafe {
                let map = BBox::new_in(AtomicU64::new(0), &self.alloc);
                let map_ptr = BBox::into_raw(map);
                let map = BBox::from_raw(map_ptr);
                let map = BBox::leak(map);
                let chunk_layout = Layout::from_size_align_unchecked(CHUNK_SIZE, CHUNK_SIZE);
                let chunk_ptr = alloc::alloc(chunk_layout);
                self.maps.push(&*map);
                self.chunks.push(ptr::NonNull::new(chunk_ptr).expect("Failed to allocate chunk"));
            }
        }
    }

    pub fn alloc_box<T>(&mut self, thing: T) -> CBox<T> {
        let layout = Layout::new::<T>();

        #[cfg(feature = "fallback-alloc")]
        if cfg!(feature = "fallback-alloc-always") || layout.size() > CHUNK_SIZE {
            static STD_ALLOC_MAP: AtomicU64 = AtomicU64::new(STD_ALLOC_BIT);
            unsafe {
                let ptr = alloc::alloc(layout) as *mut T;
                ptr::write(ptr, thing);
                let datum = ptr::NonNull::new(ptr).unwrap();
                return CBox {
                    map: &STD_ALLOC_MAP,
                    dealloc_bits: STD_ALLOC_BIT,
                    datum,
                }
            }
        }

        let (datum, map, dealloc_bits) = self.alloc_raw(layout);
        let datum = ptr::NonNull::new(datum.as_ptr().cast::<T>() as *mut _).unwrap();
        // SAFETY: `datum` is allocated and not initialized.
        unsafe {
            ptr::write(datum.as_ptr(), thing);
        }
        CBox {
            map,
            dealloc_bits,
            datum,
        }
    }

    pub fn alloc_arc<T>(&mut self, thing: T) -> CArc<T>
    where
        T: Send + Sync
    {
        let layout = Layout::new::<CArcInner<T>>();

        #[cfg(feature = "fallback-alloc")]
        if cfg!(feature = "fallback-alloc-always") || layout.size() > CHUNK_SIZE {
            static STD_ALLOC_MAP: AtomicU64 = AtomicU64::new(STD_ALLOC_BIT);
            unsafe {
                let ptr = alloc::alloc(layout) as *mut CArcInner<T>;
                let inner = CArcInner {
                    map: &STD_ALLOC_MAP,
                    dealloc_bits: STD_ALLOC_BIT,
                    strong: 1.into(),
                    weak: 1.into(),
                    datum: thing,
                };
                ptr::write(ptr, inner);
                return CArc(ptr::NonNull::new(ptr).unwrap());
            }
        }

        let (ptr, map, dealloc_bits) = self.alloc_raw(layout);
        let ptr = ptr::NonNull::new(ptr.as_ptr().cast::<T>() as *mut _).unwrap();
        let inner = CArcInner {
            map,
            dealloc_bits,
            strong: 1.into(),
            weak: 1.into(),
            datum: thing,
        };
        // SAFETY:
        // + `ptr` is allocated and not initialized.
        // + Taking a static reference to ptr.datum is safe due to guarantees
        // of CArc.
        unsafe {
            ptr::write(ptr.as_ptr(), inner);
            CArc(ptr)
        }
    }

    pub fn quiescent_cleanup(&self) {
        use Ordering::SeqCst;

        // TODO: consider doing a more fine-grained cleanup to save space.
        for map in &self.maps {
            let map_value = map.load(SeqCst);
            let bits = map_value & !ORDER_NOT_BITS[LAST_ORDER];
            if bits == 0 {
                let _ = map.compare_exchange(map_value, 0, SeqCst, SeqCst);
            }
        }
    }
}

#[cfg(feature = "dealloc-on-drop")]
impl Drop for Chunker {
    fn drop(&mut self) {
        self.maps.clear();

        unsafe {
            let layout = Layout::from_size_align_unchecked(CHUNK_SIZE, CHUNK_SIZE);
            for chunk in self.chunks.drain(..) {
                    alloc::dealloc(chunk.as_ptr(), layout);
            }

            let alloc_box = BBox::from_raw(self.alloc);
            mem::drop(alloc_box);
        }
    }
}

impl<T: ?Sized> CBox<T> {
    pub fn pin(thing: Self) -> Pin<Self> {
        // SAFETY: CBox is the single owner of this pointer.
        unsafe { Pin::new_unchecked(thing) }
    }
}

impl<T: ?Sized> Deref for CBox<T> {
    type Target = T;

    fn deref(&self) -> &T {
        // SAFETY: Pointer is valid as long as CBox is alive.
        unsafe { self.datum.as_ptr().as_ref().unwrap() }
    }
}

impl<T: ?Sized> DerefMut for CBox<T> {
    fn deref_mut(&mut self) -> &mut T {
        // SAFETY: Pointer is valid as long as CBox is alive.
        unsafe { self.datum.as_ptr().as_mut().unwrap() }
    }
}

impl<T: ?Sized> Drop for CBox<T> {
    fn drop(&mut self) {
        if self.dealloc_bits == STD_ALLOC_BIT {
            unsafe {
                let ptr = self.datum.as_ptr();
                ptr::drop_in_place(ptr);
                let layout = Layout::for_value(&*self.datum.as_ptr());
                alloc::dealloc(ptr as *mut u8, layout);
            }
        }

        // SAFETY: value behind `self.ptr` is initialized and properly
        // aligned by `Chunker::alloc`.
        unsafe { ptr::drop_in_place(self.datum.as_ptr()) };
        self.map.fetch_and(!self.dealloc_bits, Ordering::SeqCst);
    }
}

impl<T: ?Sized + Unsize<U>, U: ?Sized> CoerceUnsized<CBox<U>> for CBox<T> {}

impl<T: Send + Sync> CArc<T> {
    pub fn make_weak(&self) -> CWeak<T> {
        // SAFETY: `inner.weak` is valid while CArc exists.
        let inner = unsafe { &*self.0.as_ptr() };
        inner.weak.fetch_add(1, Ordering::SeqCst);
        CWeak(self.0)
    }
}

impl<T: Send + Sync> Drop for CArc<T> {
    fn drop(&mut self) {
        // SAFETY: `inner` is valid while CArc exists.
        let inner = unsafe { &*self.0.as_ptr() };

        if inner.strong.fetch_sub(1, Ordering::SeqCst) != 1 {
            return;
        }

        // SAFETY: at this point, CArc is the only owner of the pointer.
        // datum can never be reached after this is destroyed.
        unsafe {
            ptr::drop_in_place(&inner.datum as *const T as *mut T);
        }
        mem::drop(inner);
        mem::drop(CWeak(self.0));
    }
}

impl<T: Send + Sync> Deref for CArc<T> {
    type Target = T;

    fn deref(&self) -> &T {
        // SAFETY: `inner` is valid while CArc exists.
        let inner = unsafe { &*self.0.as_ptr() };
        &inner.datum
    }
}

unsafe impl<T: Send + Sync> Send for CArc<T> {}
unsafe impl<T: Send + Sync> Sync for CArc<T> {}

impl<T: Send + Sync> CWeak<T> {
    pub fn upgrade(self) -> Option<CArc<T>> {
        // SAFETY: `inner` is valid while CWeak exists except for `inner.datum`
        //         which may have been dropped.
        let inner = unsafe { &*self.0.as_ptr() };
        if inner.strong.load(Ordering::SeqCst) == 0 {
            return None;
        }

        let mut count = inner.strong.load(Ordering::SeqCst);

        loop {
            if count == 0 {
                return None;
            }

            match inner.strong.compare_exchange(
                count,
                count + 1,
                Ordering::SeqCst,
                Ordering::SeqCst,
            ) {
                Ok(_) => return Some(CArc(self.0)),
                Err(value) => count = value,
            }
        }
    }
}

impl<T: Send + Sync> Drop for CWeak<T> {
    fn drop(&mut self) {
        // SAFETY: `inner` is valid while CWeak exists except for `inner.datum`
        //         which may have been dropped.
        let inner = unsafe { &*self.0.as_ptr() };
        if inner.weak.fetch_sub(1, Ordering::SeqCst) == 1 {
            if inner.dealloc_bits == STD_ALLOC_BIT {
                mem::drop(inner);
                unsafe {
                    let layout = Layout::for_value(&*self.0.as_ptr());
                    alloc::dealloc(self.0.as_ptr() as *mut u8, layout);
                }
            }

            // We've already dropped the datum, so just deallocate the Inner.
            inner.map.fetch_and(!inner.dealloc_bits, Ordering::SeqCst);
        }
    }
}

unsafe impl<T: Send + Sync> Send for CWeak<T> {}
unsafe impl<T: Send + Sync> Sync for CWeak<T> {}

const STD_ALLOC_BIT: u64 = 1<<63;

const ORDER_NOT_BITS: [u64; 6] = [
    0b11111111111111111111111111111111_1111111111111111_11111111_1111_11_0,
    0b11111111111111111111111111111111_1111111111111111_11111111_1111_00_1,
    0b11111111111111111111111111111111_1111111111111111_11111111_0000_11_1,
    0b11111111111111111111111111111111_1111111111111111_00000000_1111_11_1,
    0b11111111111111111111111111111111_0000000000000000_11111111_1111_11_1,
    0b00000000000000000000000000000000_1111111111111111_11111111_1111_11_1,
];

const ORDER_CHILDREN_BITS: [u64; 6] = [
    0b11111111111111111111111111111111_1111111111111111_11111111_1111_11_1,
    0b11111111111111111111111111111111_1111111111111111_11111111_1111_11_0,
    0b11111111111111111111111111111111_1111111111111111_11111111_1111_00_0,
    0b11111111111111111111111111111111_1111111111111111_11111111_0000_00_0,
    0b11111111111111111111111111111111_1111111111111111_00000000_0000_00_0,
    0b11111111111111111111111111111111_0000000000000000_00000000_0000_00_0,
];

const ALLOCATION_BITS: [u64; 63] = [
    0b11111111111111111111111111111111_1111111111111111_11111111_1111_11_1,
    0b00000000000000001111111111111111_0000000011111111_00001111_0011_01_1,
    0b11111111111111110000000000000000_1111111100000000_11110000_1100_10_1,
    0b00000000000000000000000011111111_0000000000001111_00000011_0001_01_1,
    0b00000000000000001111111100000000_0000000011110000_00001100_0010_01_1,
    0b00000000111111110000000000000000_0000111100000000_00110000_0100_10_1,
    0b11111111000000000000000000000000_1111000000000000_11000000_1000_10_1,
    0b00000000000000000000000000001111_0000000000000011_00000001_0001_01_1,
    0b00000000000000000000000011110000_0000000000001100_00000010_0001_01_1,
    0b00000000000000000000111100000000_0000000000110000_00000100_0010_01_1,
    0b00000000000000001111000000000000_0000000011000000_00001000_0010_01_1,
    0b00000000000011110000000000000000_0000001100000000_00010000_0100_10_1,
    0b00000000111100000000000000000000_0000110000000000_00100000_0100_10_1,
    0b00001111000000000000000000000000_0011000000000000_01000000_1000_10_1,
    0b11110000000000000000000000000000_1100000000000000_10000000_1000_10_1,
    0b00000000000000000000000000000011_0000000000000001_00000001_0001_01_1,
    0b00000000000000000000000000001100_0000000000000010_00000001_0001_01_1,
    0b00000000000000000000000000110000_0000000000000100_00000010_0001_01_1,
    0b00000000000000000000000011000000_0000000000001000_00000010_0001_01_1,
    0b00000000000000000000001100000000_0000000000010000_00000100_0010_01_1,
    0b00000000000000000000110000000000_0000000000100000_00000100_0010_01_1,
    0b00000000000000000011000000000000_0000000001000000_00001000_0010_01_1,
    0b00000000000000001100000000000000_0000000010000000_00001000_0010_01_1,
    0b00000000000000110000000000000000_0000000100000000_00010000_0100_10_1,
    0b00000000000011000000000000000000_0000001000000000_00010000_0100_10_1,
    0b00000000001100000000000000000000_0000010000000000_00100000_0100_10_1,
    0b00000000110000000000000000000000_0000100000000000_00100000_0100_10_1,
    0b00000011000000000000000000000000_0001000000000000_01000000_1000_10_1,
    0b00001100000000000000000000000000_0010000000000000_01000000_1000_10_1,
    0b00110000000000000000000000000000_0100000000000000_10000000_1000_10_1,
    0b11000000000000000000000000000000_1000000000000000_10000000_1000_10_1,
    0b00000000000000000000000000000001_0000000000000001_00000001_0001_01_1,
    0b00000000000000000000000000000010_0000000000000001_00000001_0001_01_1,
    0b00000000000000000000000000000100_0000000000000010_00000001_0001_01_1,
    0b00000000000000000000000000001000_0000000000000010_00000001_0001_01_1,
    0b00000000000000000000000000010000_0000000000000100_00000010_0001_01_1,
    0b00000000000000000000000000100000_0000000000000100_00000010_0001_01_1,
    0b00000000000000000000000001000000_0000000000001000_00000010_0001_01_1,
    0b00000000000000000000000010000000_0000000000001000_00000010_0001_01_1,
    0b00000000000000000000000100000000_0000000000010000_00000100_0010_01_1,
    0b00000000000000000000001000000000_0000000000010000_00000100_0010_01_1,
    0b00000000000000000000010000000000_0000000000100000_00000100_0010_01_1,
    0b00000000000000000000100000000000_0000000000100000_00000100_0010_01_1,
    0b00000000000000000001000000000000_0000000001000000_00001000_0010_01_1,
    0b00000000000000000010000000000000_0000000001000000_00001000_0010_01_1,
    0b00000000000000000100000000000000_0000000010000000_00001000_0010_01_1,
    0b00000000000000001000000000000000_0000000010000000_00001000_0010_01_1,
    0b00000000000000010000000000000000_0000000100000000_00010000_0100_10_1,
    0b00000000000000100000000000000000_0000000100000000_00010000_0100_10_1,
    0b00000000000001000000000000000000_0000001000000000_00010000_0100_10_1,
    0b00000000000010000000000000000000_0000001000000000_00010000_0100_10_1,
    0b00000000000100000000000000000000_0000010000000000_00100000_0100_10_1,
    0b00000000001000000000000000000000_0000010000000000_00100000_0100_10_1,
    0b00000000010000000000000000000000_0000100000000000_00100000_0100_10_1,
    0b00000000100000000000000000000000_0000100000000000_00100000_0100_10_1,
    0b00000001000000000000000000000000_0001000000000000_01000000_1000_10_1,
    0b00000010000000000000000000000000_0001000000000000_01000000_1000_10_1,
    0b00000100000000000000000000000000_0010000000000000_01000000_1000_10_1,
    0b00001000000000000000000000000000_0010000000000000_01000000_1000_10_1,
    0b00010000000000000000000000000000_0100000000000000_10000000_1000_10_1,
    0b00100000000000000000000000000000_0100000000000000_10000000_1000_10_1,
    0b01000000000000000000000000000000_1000000000000000_10000000_1000_10_1,
    0b10000000000000000000000000000000_1000000000000000_10000000_1000_10_1,
];

