//! Utilities for manipulating executor and tick state.

use std::{
    cell::UnsafeCell,
    future::Future,
    ptr,
};
use crate::{
    SpawnContext, JoinHandle,
    unchecked_any::UncheckedAny,
    universal_type_id::UniversalTypeId,
};

#[derive(Clone, Copy)]
struct BroadcastState {
    spawner: ptr::NonNull<SpawnContext>,
    extra: UncheckedAny,
    await_condition: AwaitCondition,
    access_mode: AccessMode,
}

#[repr(usize)]
#[derive(Clone, Copy, PartialEq, Eq)]
pub(crate) enum AwaitCondition {
    NextTick = 0,
    Exclusive = 1,
}

#[repr(usize)]
#[derive(Clone, Copy, PartialEq, Eq)]
pub(crate) enum AccessMode {
    Shared = 0,
    Exclusive = 1,
}

thread_local! {
    static STATE: UnsafeCell<Option<BroadcastState>> = UnsafeCell::new(None);
}

/// Perform `action` on a tick state state immutably accessed by this conduit.
pub fn with<T, F, R>(action: F) -> R
where
    T: UniversalTypeId,
    F: FnOnce(&T) -> R
{
    unsafe {
        STATE.with(|s| {
            let extra = (&*s.get()).as_ref().unwrap().extra;
            action(&*extra.downcast_ref_unchecked())
        })
    }
}

/// Perform `action` on the state exclusively accessed by this conduit.
///
/// Returns a future that, upon awaiting on it, puts the fiber into
/// an "exclusive access" mode, guaranteeing that no other fibers are
/// using a tick state.
pub async fn with_mut<T, F, R>(action: F) -> R
where
    T: UniversalTypeId,
    F: FnOnce(&mut T) -> R
{
    STATE.with(|s| unsafe {
        (&mut*s.get()).as_mut().unwrap().await_condition = AwaitCondition::Exclusive;
    });

    crate::yield_now().await;

    STATE.with(|s| unsafe {
        let state = (&mut*s.get()).as_mut().unwrap();
        let access_mode = state.access_mode;
        if access_mode != AccessMode::Exclusive {
            panic!("State access mode is not exclusive when using ExclusiveConduit");
        }

        let extra = state.extra;
        action(&mut *extra.downcast_ref_unchecked())
    })
}

/// Spawn a new future in the current executor.
pub fn spawn<F>(task: F)
where
    F: Future<Output=()> + Send + Sync + 'static
{
    unsafe {
        STATE.with(|s| (&mut*s.get()).as_mut().unwrap().spawner.as_mut().spawn(task))
    }
}

/// Spawn a new future in the current executor. Allows awaiting for completion.
pub fn spawn_ret<F, T>(task: F) -> JoinHandle<T>
where
    F: Future<Output=T> + Send + Sync + 'static,
    T: Send + 'static
{
    unsafe {
        STATE.with(|s| (&mut*s.get()).as_mut().unwrap().spawner.as_mut().spawn_ret(task))
    }
}

pub(crate) unsafe fn set_broadcast<T>(spawner: &mut SpawnContext, state: &mut T)
where T: UniversalTypeId {
    STATE.with(|s| {
        let bs = BroadcastState {
            spawner: ptr::NonNull::new(spawner).unwrap(),
            extra: UncheckedAny::new(state),
            await_condition: AwaitCondition::NextTick,
            access_mode: AccessMode::Shared,
        };
        *s.get() = Some(bs);
    });
}

pub(crate) unsafe fn read_await_condition() -> AwaitCondition {
    STATE.with(|s| {
        let state = (&mut*s.get()).as_mut().unwrap();
        let condition = state.await_condition;
        state.await_condition = AwaitCondition::NextTick;
        condition
    })
}

pub(crate) unsafe fn set_access_mode(access_mode: AccessMode) {
    STATE.with(|s| {
        (&mut*s.get()).as_mut().unwrap().access_mode = access_mode;
    })
}

pub(crate) unsafe fn reset_broadcast() {
    STATE.with(|s| {
        *s.get() = None;
    })
}

