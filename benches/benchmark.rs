use criterion::{black_box, criterion_group, criterion_main, Criterion, BatchSize};
use lofi::{self, Executor, impl_universal_type_id, state};

pub fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("insert 100", |b| b.iter_batched(make_executor, |mut ex| insert_n(&mut ex, black_box(100)), BatchSize::LargeInput));
    c.bench_function("insert 1000", |b| b.iter_batched(make_executor, |mut ex| insert_n(&mut ex, black_box(1000)), BatchSize::LargeInput));
    c.bench_function("insert 5000", |b| b.iter_batched(make_executor, |mut ex| insert_n(&mut ex, black_box(5000)), BatchSize::LargeInput));
    c.bench_function("insert 100, tick", |b| b.iter_batched(make_executor, |mut ex| insert_n_tick(&mut ex, black_box(100)), BatchSize::LargeInput));
    c.bench_function("insert 1000, tick", |b| b.iter_batched(make_executor, |mut ex| insert_n_tick(&mut ex, black_box(1000)), BatchSize::LargeInput));
    c.bench_function("insert 5000, tick", |b| b.iter_batched(make_executor, |mut ex| insert_n_tick(&mut ex, black_box(5000)), BatchSize::LargeInput));
    c.bench_function("insert 100, tick, conduit", |b| b.iter_batched(make_executor, |mut ex| insert_n_tick_conduit(&mut ex, black_box(100)), BatchSize::LargeInput));
    c.bench_function("insert 1000, tick, conduit", |b| b.iter_batched(make_executor, |mut ex| insert_n_tick_conduit(&mut ex, black_box(1000)), BatchSize::LargeInput));
    c.bench_function("insert 5000, tick, conduit", |b| b.iter_batched(make_executor, |mut ex| insert_n_tick_conduit(&mut ex, black_box(5000)), BatchSize::LargeInput));
    c.bench_function("insert 100, yield, tick", |b| b.iter_batched(make_executor, |mut ex| insert_n_yield_tick(&mut ex, black_box(100)), BatchSize::LargeInput));
    c.bench_function("insert 1000, yield, tick", |b| b.iter_batched(make_executor, |mut ex| insert_n_yield_tick(&mut ex, black_box(1000)), BatchSize::LargeInput));
    c.bench_function("insert 5000, yield, tick", |b| b.iter_batched(make_executor, |mut ex| insert_n_yield_tick(&mut ex, black_box(5000)), BatchSize::LargeInput));
}

fn insert_n(executor: &mut Executor, count: usize) {
    for _ in 0..count {
        executor.insert(async {});
    }
}

fn insert_n_tick(executor: &mut Executor, count: usize) {
    for _ in 0..count {
        executor.insert(async {});
    }
    executor.tick(&mut ());
}

fn insert_n_tick_conduit(executor: &mut Executor, count: usize) {
    for _ in 0..count {
        executor.insert(async move {
            let mut n = 0;
            state::with(|state: &State| {
                n = state.n;
            });
            black_box(n);
        });
    }
    executor.tick(&mut State { n: 9 });
}

fn insert_n_yield_tick(executor: &mut Executor, count: usize) {
    for _ in 0..count {
        executor.insert(async {lofi::yield_now().await});
    }
    executor.tick(&mut ());
    executor.tick(&mut ());
}

fn make_executor() -> Executor {
    Executor::new()
}

pub struct State {
    n: usize,
}
impl_universal_type_id!(State<>);

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
